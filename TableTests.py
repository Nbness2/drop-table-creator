from util import misc
from ItemTable import ItemTable


def test_ugly_array(ugly_array):
    cute_table = ItemTable.autotable(ugly_array)
    return cute_table


def test_java_to_python(java_array):
        j2p_table = ItemTable.from_java(java_array)
        return j2p_table


def array_pack_unpack_test():
    test_array = test_java_to_python(misc.test_java_array)
    print(len(test_array.table_name))
    packed = bytes(test_array)
    unpacked = ItemTable.unpack(packed)
    print(test_array == unpacked)


if __name__ == "__main__":
    array_pack_unpack_test()
    j2p_array = test_java_to_python(misc.test_java_array)
    u2c_array = test_ugly_array(misc.test_ugly_array)
    print(j2p_array.brackets[0] == u2c_array.brackets[0])
    print(j2p_array, u2c_array)
    for jbracket_id, ubracket_id in zip(j2p_array.brackets, u2c_array.brackets):
        jbracket = j2p_array.brackets[jbracket_id].item_ids
        u2cbracket = u2c_array.brackets[ubracket_id].item_ids
        print(jbracket, u2cbracket)
        print(jbracket == u2cbracket)