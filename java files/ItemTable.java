package *.here;

import *.RarityBracket;

public class ItemTable {
    private String tableName;
    private int totalSlots;
    private RarityBracket[] rarityBrackets;
    private byte[] slots;
    private boolean available;

    public ItemTable(String tableName, int totalSlots, RarityBracket[] rarityBrackets){

        this.tableName = tableName;
        this.totalSlots = totalSlots;
        this.rarityBrackets = rarityBrackets;
        int j = 0;
        int bracketSlotsCounted = 0;

        for (RarityBracket rb: this.rarityBrackets){
            rb.setBracketId((byte) j);
            j++;
            bracketSlotsCounted += rb.getAllocatedSlots();
        }

        if (bracketSlotsCounted != this.totalSlots){
            System.out.println("Invalid slot count for ItemTable "+this.tableName+", ("+bracketSlotsCounted+"/"+this.totalSlots+").");
            this.available = false;
        } else {
            this.available = true;
        }

        if (this.available) {
            j = 0;
            this.slots = new byte[totalSlots];

            for (RarityBracket rb : this.rarityBrackets) {
                for (int i = 0; i < rb.getAllocatedSlots(); i++) {
                    this.slots[j] = rb.getBracketId();
                    j++;
                }

            }
        }

    }

    public String getTableName(){ return this.tableName; }

    public int getTotalSlots(){ return this.totalSlots; }

    public boolean hasItem(int id){

        if (this.available) {

            for (RarityBracket rb : this.rarityBrackets) {
                if (rb.hasItem(id)) return true;
            }

        }

        return false;
    }

    public RarityBracket getBracketForItem(int itemId){

        for (RarityBracket rb : this.rarityBrackets) {
            if (rb.hasItem(itemId)) return rb;
        }

        return null;
    }

    public byte getBracketIdForItem(int itemId){

        RarityBracket bracket = getBracketForItem(itemId);
        if (bracket != null){ return bracket.getBracketId(); }
        return -1;
    }

    public String getBracketNameForItem(int itemId){

        RarityBracket bracket = getBracketForItem(itemId);
        if (bracket != null){ return bracket.getBracketName(); };
        return "null bracket";
    }

    public short pickItemId(){

        if (this.available) { // this does this
            byte slotteryChoice = this.slots[(int) Math.floor(Math.random() * this.slots.length)];
            RarityBracket luckyBracket = this.rarityBrackets[slotteryChoice];
            return luckyBracket.pickItem();
        }

        return (short) -1;
    }

    public short[] pickItemIds(int amount){

        if (this.available){
            short[] pickedItems = new short[amount];

            for (int i=0; i<amount; i++){
                pickedItems[i] = this.pickItemId();
            }

            return pickedItems;
        }

        return new short[] {-1};
    }

}
