package server.model.content;
import server.model.content.RarityBracket;
import server.model.content.ItemTable;

public class ItemTables {
    public static final ItemTable PVP_DROP_TABLE = new ItemTable("Pvp Drop", 113,
            new RarityBracket[] {
                    new RarityBracket("SUPER COMMON", 24, new short[] {6735} ),
                    new RarityBracket("VERY COMMON", 19, new short[] {3140, 4708, 4710, 3176, 1187, 19281, 11732, 4585, 4087} ),
                    new RarityBracket("COMMON", 16, new short[] {4151, 15020, 6739, 4716, 4722, 4726, 4734, 4755, 12985, 1771, 19284, 13734} ),
                    new RarityBracket("KINDA UNCOMMON", 14, new short[] {6731, 6733, 4714, 4718, 4724, 4734, 4753, 12988, 19287, 19339, 11690} ),
                    new RarityBracket("UNCOMMON", 11, new short[] {10296, 11335, 4712, 4728, 4736, 4738, 4745, 8842, 4747, 12991, 1765, 15259, 11708, 11718} ),
                    new RarityBracket("VERY UNCOMMON", 8, new short[] {6737, 8839, 8840, 14479, 4751, 4757, 12994, 19290, 19299, 17273, 11704, 11722, 13263} ),
                    new RarityBracket("SUPER UNCOMMON", 6, new short[] {13864, 13867, 4720, 4730, 4749, 4759, 15018, 15019, 12997, 1767, 1772, 17726, 11663, 11664, 11665, 11706, 11720} ),
                    new RarityBracket("KINDA RARE", 5, new short[] {13861, 13873, 13876, 1165, 14484, 15220, 13000, 14637, 19302, 19338, 19340, 18346, 11702} ),
                    new RarityBracket("RARE", 4, new short[] {11808, 13858, 13870, 13890, 13893, 13896, 13926, 13929, 15017, 15098, 10551, 19336, 19337, 13752, 15825} ),
                    new RarityBracket("VERY RARE", 3, new short[] {13884, 13887, 13902, 13923, 13978, 13003, 19669, 19293, 6570, 13746} ),
                    new RarityBracket("SUPER RARE", 2, new short[] {13899, 13905, 15497, 14579, 19341, 19342, 19344, 19343, 19345, 13750, 13754} ),
                    new RarityBracket("LEGENDARY", 1, new short[] {11724, 13006, 18747, 19305, 13748} ),
            }
    );
}
