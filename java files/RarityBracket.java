package *.here;

import *.Misc;

public class RarityBracket {

    private String bracketName;
    private int allocatedSlots;
    private short[] itemIds;
    private byte bracketId;

    public RarityBracket(String bracketName, int allocatedSlots, short[] itemIds){

        this.bracketName = bracketName;
        this.allocatedSlots = allocatedSlots;
        this.itemIds = itemIds;
    }

    public void setBracketId(byte id){
        this.bracketId = id;
    }

    public String getBracketName(){ return this.bracketName; }

    public byte getBracketId(){
        return this.bracketId;
    }

    public int getAllocatedSlots() { return this.allocatedSlots; }

    public boolean hasItem(int item_id){ return Misc.arrayContains(this.itemIds, item_id); }

    public short pickItem(){

        short pickedItem = this.itemIds[(int) Math.floor(Math.random() * this.itemIds.length)];
        return pickedItem;
    }

    public short[] pickItems(int amount){

        short[] pickedItems = new short[amount];

        if (!(amount <= 0)) {

            for (int i = 0; i < amount; i++) {
                pickedItems[i] = this.pickItem();
            }
        }
        return pickedItems;
    }
}
