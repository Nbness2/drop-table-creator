import random
from RarityBracket import RarityBracket
from util import misc
from util import TableUtils


class ItemTable:

    def __init__(self, table_name: str,  allocated_rarity_slots: int):

        self.table_name = misc.confirm_data_type(table_name, str, "ItemTable.table_name")[:128]
        self.allocated_rarity_slots = misc.confirm_data_type(allocated_rarity_slots, int, "ItemTable.allocated_rarity_slots")
        self.inited = False
        self.slots = None
        self.brackets = {}

    def __str__(self):

        retstr = "ItemTable(\"{}\", {}, ".format(self.table_name, self.allocated_rarity_slots)
        retstr += "{}".format(tuple(str(self.brackets[bracket_id]) for bracket_id in self.brackets))
        return retstr

    def __contains__(self, item):

        for bracket_id in self.brackets:

            if item in self.brackets[bracket_id]:
                return True
        return False

    def __eq__(self, other):
        """checks for statistical equality between tables. good for detecting duplicates."""
        if not self.allocated_rarity_slots == other.allocated_rarity_slots:
            return False

        if not self.slots == other.slots:
            return False

        if not self.brackets == other.brackets:
            return False
        return True

    def __bytes__(self):
        """packs the table into the latest version bytestring"""
        bytestring = TableUtils.pack_to_bits(self)
        return bytestring

    def pack(self, version):

        bytestring = TableUtils.pack_to_bits(self, version)
        return bytestring

    @staticmethod
    def unpack(bytestring: bytes):

        table = TableUtils.unpack_table_from_bits(bytestring)
        return table

    def set_table_name(self, new_name: str):

        misc.confirm_data_type(new_name, str, "ItemTable.table_name")
        self.table_name = new_name[:128]

    def init(self):

        self.inited = True
        self.slots = []

        for bracket_id in self.brackets:
            self.slots.extend([bracket_id]*self.brackets[bracket_id].allocated_slots)

    def pick_item(self, amount=1):

        if amount == 1:
            bracket_id = random.SystemRandom().choice(self.slots)
            return self.brackets[bracket_id].pick_item()
        items = []

        for item in range(amount):
            bracket_id = random.SystemRandom().choice(self.slots)
            items.append(self.brackets[bracket_id].pick_item())
        return tuple(items)

    def bracket_for_item_id(self, item_id):

        for bracket_id in self.brackets:

            if item_id in self.brackets[bracket_id]:
                return bracket_id
        return False

    def add_bracket(self, rarity_bracket: RarityBracket):
        """
        :type rarity_bracket: RarityBracket
        """

        if self.inited:
            print("Cannot change an already initiated ItemTable")
        else:
            rarity_bracket.set_id(len(self.brackets))
            misc.confirm_data_type(rarity_bracket, RarityBracket, "ItemTable.brackets[bracket]")
            self.brackets[rarity_bracket.bracket_id] = rarity_bracket

    def to_java(self):

        return TableUtils.to_java(self)

    @staticmethod
    def from_java(string: str):

        return TableUtils.from_java(string)

    @staticmethod
    def create():

        new_table_name = misc.advanced_input(
            message="Input a name for this table: ",
            field="table name",
            mustconfirm=False,
            expected_datatype=str
        )
        allocated_slots = misc.advanced_input(
            message="Input the amount of slots you are allocating to table {}: ".format(new_table_name),
            field="allocated slots",
            mustconfirm=False,
            expected_datatype=int,
            greaterthan=0,
            lessthan=536870912
        )
        new_table = ItemTable(table_name=new_table_name, allocated_rarity_slots=allocated_slots)

        while allocated_slots:
            new_bracket = RarityBracket.create(allocated_slots)
            print("created rarity {} with {} slots".format(new_bracket.bracket_name, new_bracket.allocated_slots))
            allocated_slots -= new_bracket.allocated_slots
            new_table.add_bracket(new_bracket)
        new_table.init()
        return new_table

    @staticmethod
    def autotable(uglyarray: str, big_endian: bool = True, sort_ids: bool = False):
        """
        :param uglyarray:
            Copy paste your java array into autotable function. Example array: {1, 2, 3, 4, 5}
        :param big_endian:
            if true, brackets will be put in descending order (biggest slot bracket first), vice versa
        :param sort_ids:
            if true, will sort the ids in numerical order after splitting brackets up.
        :return:
        """

        uglyarray = uglyarray.replace("{", "").replace("}", "").replace(" ", "")
        uglyarray = [int(item_id) for item_id in uglyarray.split(",")]
        bare_brackets = {}

        for unique_id in set(uglyarray):
            bracket_slots = uglyarray.count(unique_id)

            if bracket_slots not in bare_brackets:
                bare_brackets[bracket_slots] = []
            bare_brackets[bracket_slots].append(unique_id)

        if sort_ids:

            for bracket_id in bare_brackets:
                bare_brackets[bracket_id] = sorted(bare_brackets[bracket_id])

        total_slots = sum(bare_brackets.keys())
        table = ItemTable("{}_SLOT_TABLE".format(total_slots), total_slots)
        bracket_keys = bare_brackets.keys()

        if big_endian:
            bracket_keys = reversed(list(bracket_keys))

        for bracket_slots in bracket_keys:
            table.add_bracket(
                RarityBracket(
                    bracket_name="{}_SLOT_BRACKET".format(bracket_slots),
                    allocated_slots=bracket_slots,
                    item_ids=tuple(bare_brackets[bracket_slots])
                )
            )
        return table


def validate_table(table: ItemTable) -> bool:

    total_bracket_slots = 0

    for bracket_id in table.brackets:
        total_bracket_slots += table.brackets[bracket_id].allocated_slots

    if total_bracket_slots != table.allocated_rarity_slots:
        return False
    return True