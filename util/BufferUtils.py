from util import misc


class ByteBuffer(object):
    def __init__(self, byte_array=b''):
        self.buffer = byte_array

    def __bool__(self):
        return bool(len(self.buffer))

    def __len__(self):
        return len(self.buffer)

    def __setitem__(self, key, value):
        if type(key) is not int:
            raise TypeError("Cannot locate index with type {}".format(type(key)))
        if key < 0:
            raise IndexError("Cannot set value to negative index")
        if key > len(self.buffer)-1:
            raise IndexError("Index {} out of range".format(key))
        if type(value) not in (bytes, int):
            raise TypeError("Cannot convert {} to a single byte".format(type(value)))
        if type(value) is bytes:
            if ord(value) > 255:
                raise ValueError("Must set 1-byte values only, not {}-byte".format(len(value)))
        else:
            if -128 > value > 255:
                raise ValueError("Cannot overflow, must set 1-byte values only")
            value = value.to_bytes(1, 'little', signed=True)
        self.buffer = self.buffer[:key] + value + self.buffer[key+1:]

    def set_byte(self, pos, val):
        self[pos] = val

    def __str__(self):
        return ''.join([chr(char) for char in self.buffer])

    def read_byte(self, byteorder="big", signed=False):
        _byte = self._get_arbitrary(1, byteorder, signed)
        return _byte

    def read_short(self, byteorder="big", signed=False):
        _short = self._get_arbitrary(2, byteorder, signed)
        return _short

    def read_char(self, byteorder="big", signed=False):
        _char = self._get_arbitrary(2, byteorder, signed)
        return _char

    def read_tribyte(self, byteorder="big", signed=False):
        _tribyte = self._get_arbitrary(3, byteorder, signed)
        return _tribyte

    def read_int(self, byteorder="big", signed=False):
        _int = self._get_arbitrary(4, byteorder, signed)
        return _int

    def read_float(self, byteorder="big", signed=False):
        _float = self._get_arbitrary(4, byteorder, signed)
        return _float

    def read_double(self, byteorder="big", signed=False):
        _double = self._get_arbitrary(8, byteorder, signed)
        return _double

    def read_long(self, byteorder="big", signed=False):
        _long = self._get_arbitrary(8, byteorder, signed)
        return _long

    def _get_arbitrary(self, bytecount, byteorder="big", signed=False, return_bytes=False):
        if self.length() > 0:
            if byteorder.lower() in ("big", "little"):
                byte_value = self.buffer[:bytecount]
                if return_bytes:
                    return byte_value
                self.buffer = self.buffer[bytecount:]
                return int.from_bytes(byte_value, byteorder=byteorder, signed=signed)
            elif byteorder.lower() in ("m", "mid", "middle"):
                pass
        raise IndexError("Cannot get bytes from empty buffer {}, {}".format(bytecount, self.buffer))

    def read_string(self):
        if self.length() > 0:
            string = self.buffer.split(b'\n')[0]
            _len = len(string) + 1
            self.buffer = self.buffer[_len:]
            return string.decode()

    def _write_arbitrary(self, data, forced_size=None, *, byte_order='big', signed=False):
        if not forced_size:
            forced_size = misc.length(data)
        if type(data) is bytes:
            self.buffer += data
        elif type(data) is int:
            self.buffer += data.to_bytes(forced_size, byteorder=byte_order, signed=signed)
        elif type(data) is str:
            self.buffer += data[:forced_size].encode('utf-8')
        else:
            raise TypeError("Cannot directly pack {} into bytes".format(type(data)))

    def write_byte_a(self, data):
        if misc.length(data) == 1:
            self._write_arbitrary(data+128, 1)

    def write_byte_s(self, data):
        if misc.length(data) == 1:
            self._write_arbitrary(128-data, 1)

    def write_byte_c(self, data):
        if misc.length(data) == 1:
            self._write_arbitrary(-data, 1, signed=True)

    def write_byte(self, data, signed=False):
        if misc.length(data) > 1:
            raise ValueError("Cannot pack data of length {} into byte".format(misc.length(data)))
        self._write_arbitrary(data, 1, signed=signed)

    write_opcode = write_byte

    def write_short(self, data, byte_order='big', signed=False):
        if misc.length(data) > 2:
            raise ValueError("Cannot pack data of length {} into short".format(misc.length(data)))
        self._write_arbitrary(data, 2, byte_order=byte_order, signed=signed)

    def write_int(self, data, byte_order='big', signed=False):
        if misc.length(data) > 4:
            raise ValueError("Cannot pack data of length {} into int".format(misc.length(data)))
        self._write_arbitrary(data, 4, byte_order=byte_order, signed=signed)

    write_word = write_int

    def write_long(self, data, byte_order='big', signed=False):
        if misc.length(data) > 8:
            raise ValueError("Cannot pack data of length {} into long".format(misc.length(data)))
        self._write_arbitrary(data, 8, byte_order=byte_order, signed=signed)

    write_dword = write_long

    def write_qword(self, data, byte_order='big', signed=False):
        if misc.length(data) > 16:
            raise ValueError("Cannot pack data of length {} into qword".format(misc.length(data)))
        self._write_arbitrary(data, 16, byte_order=byte_order, signed=signed)

    write_dlong = write_qword

    def write_bytes(self, data):
        if not type(data) is bytes:
            raise TypeError("Cannot write bytes from data type {}{}".format(type(data),
                                                                            ", try write_string for this data" if type(
                                                                                data) is str else ""))
        self._write_arbitrary(data)

    def write_string(self, data):
        if not type(data) is str:
            raise TypeError("Cannot write string from data type {}".format(type(data)))
        self._write_arbitrary(data)
        self.write_byte(b'\n')

    def flush(self):
        self.buffer = b''

    def append_bytes(self, byte):
        self.buffer += byte

    def take_data(self):
        data = self.buffer
        self.flush()
        return data

    def get_buffer(self):
        return self.buffer

    get_data = get_buffer

    def length(self):
        return misc.length(self.buffer)

    def dump_string(self):
        return "".join(chr(char) for char in self.take_data())


class BitBuffer:
    def __init__(self, byte_array=b'', padding=b'0',
                 auto_update=False, auto_pad=False, auto_newbyte=True, return_separate_bytes=False, return_type=list
                 ):
        if type(byte_array) is bytes:
            pass
        elif type(byte_array) is str:
            byte_array = b''.join(ord(char).to_bytes(1, 'little') for char in byte_array)
        elif type(byte_array) is list:
            byte_array = bytes(byte_array)
        elif type(byte_array) is int:
            byte_array = byte_array.to_bytes(misc.length(byte_array), 'little')
        self.auto_update = bool(auto_update)
        self.auto_pad = bool(auto_pad)
        self.return_type = return_type
        self.auto_newbyte = bool(auto_newbyte)
        self.return_separate_bytes = return_separate_bytes
        self.bit_buffer = b''
        self.byte_buffer = byte_array
        self.padding = padding
        self.byte_pointer = len(self.byte_buffer) - 1 if len(self.byte_buffer) else 0
        self.bit_pointer = 0
        self.bit_buffer = []
        for byte_idx, byte in enumerate(self.byte_buffer):
            bits = bin(byte)[2:].encode()
            if self.auto_pad and not byte_idx == len(self.byte_buffer)-1:
                self.bit_buffer.append(bits.rjust(8, self.padding))
            else:
                self.bit_buffer.append(bits)

    def set_buffer(self, byte_array, auto_pad=False):
        if type(byte_array) is bytes:
            pass
        elif type(byte_array) is str:
            byte_array = b''.join(ord(char).to_bytes(1, 'little') for char in byte_array)
        elif type(byte_array) is list:
            byte_array = bytes(byte_array)
        elif type(byte_array) is int:
            byte_array = byte_array.to_bytes(misc.length(byte_array), 'little')
        self.auto_pad = auto_pad
        self.bit_buffer = b''
        self.byte_buffer = byte_array
        self.byte_pointer = len(self.byte_buffer) - 1 if len(self.byte_buffer) else 0
        self.bit_pointer = 0
        self.bit_buffer = []
        self.byte_buffer = byte_array
        for byte_idx, byte in enumerate(self.byte_buffer):
            bits = bin(byte)[2:].encode()
            if self.auto_pad and not byte_idx == len(self.byte_buffer)-1:
                self.bit_buffer.append(bits.rjust(8, self.padding))
            else:
                self.bit_buffer.append(bits)

    def __str__(self):
        return str(self.bit_buffer)

    def __bytes__(self):
        self.update_byte_buffer()
        return b"".join([char.to_bytes(1, "big") for char in self.byte_buffer])

    def write_bit(self, value, amount=1):
        """
        write_bit
            write_bit writes a bit to the buffer corresponding to the bool() result of value.

        :param value:
            The value of the bit that you are writing to the bit_buffer. Written bit is equal to the bool() result of
                value.

        :param amount:
            the amount of bits you want to write the value in.

        :return None:
        """
        byte_format = '{0:0' + str(amount) + 'b}'
        try:
            bit_value = byte_format.format(value)
        except TypeError:
            print(value)
            exit()

        if len(bit_value) > amount:
            raise ValueError(
                "{} can not be written in length of {} bits, expected size [{}]".format(value, amount, len(bit_value)))

        for bit in bit_value:

            if len(self.bit_buffer) == 0:
                self.bit_buffer.append(b''.rjust(8, self.padding) if self.auto_pad else b'')

            if self.bit_pointer < 8:
                self.bit_buffer[self.byte_pointer] = misc.replace_str_char(self.bit_buffer[self.byte_pointer],
                                                                           self.bit_pointer,
                                                                           b'1' if bit == '1' else b'0')
                self.bit_pointer += 1
            else:
                self.bit_pointer = 0
                if self.auto_newbyte:
                    self.new_byte()
                else:
                    self.byte_pointer = 0
                self.write_bit(int(bit))

            if self.auto_update:
                self.update_byte_buffer()

    def new_byte(self):
        self.bit_pointer = 0
        self.byte_pointer += 1
        self.bit_buffer.append(b''.rjust(8, self.padding) if self.auto_pad else b'0')

    def __len__(self):
        return len(self.bit_buffer)

    def __setitem__(self, key, value):
        """
        __setitem__
            __setitem__ can use either number or slices as an argument to set bit values.

        :param key:
            key is used to access and set bit and byte pointers.  If an int argument is given to key,
            (example: BitBuffer[1]), the byte_pointer will be set to key and bit_pointer will be set to 0 using
            set_byte_pointer.If a slice is given to key (example: BitBuffer[2:6]), the byte_pointer will be set to
                slice.start using set_byte_pointer, and bit_pointer will be set to slice.stop using set_bit_pointer.
                slice.step will be ignored.

        :param value:
            value is the value that is assigned to the given bit and byte pointers. If key is an int, it is assumed you
                are replacing the whole byte, so a 1 byte value (int, char in ord range 0-255, bytes) will be needed. If
                the key is a slice, any value with a bool() result can be used.

        :return None:
        """
        if type(key) is slice:
            self.byte_pointer = key.start
            self.bit_pointer = key.stop
            self.write_bit(value)
        elif type(key) is int:
            self.byte_pointer = key
            if type(value) is int:
                value = value
            elif type(value) is str:
                try:
                    value = ord(value[0])
                except IndexError:
                    return
            elif type(value) is bytes:
                try:
                    value = value[0]
                except IndexError:
                    return
            else:
                return
            if 0 <= value < 256:
                self.bit_buffer[key] = bin(value)[2:].encode().rjust(8, b'0')
            else:
                raise ValueError("value must be in range [0, 256)")
        else:
            raise TypeError('Indices must be int or slice, not {}'.format(type(key)))

    # TODO: Decide if <item=int> should return the whole byte, or the bit with that index.
    # TODO: For example, BitBuffer[14] being equivalent to BitBuffer[2:6]
    def __getitem__(self, item):
        """
        __getitem__
            __getitem__ can use either slice or int for item.

        :param item:
            item can be either a slice or an int. If a slice is given, item.start will be the byte that the bit is being
                fetched from, and item.stop will be the bit that will be fetched. If an int is given, the corresponding
                byte will be returned.

        :return bool, str:
        """
        if type(item) is slice:
            return True if self.bit_buffer[item.start][item.stop] == 49 else False
        elif type(item) is int:
            return self.bit_buffer[item]
        else:
            raise TypeError('Indices must be int or slice, not {}'.format(type(item)))

    def flip_byte(self, byte_idx):
        self.bit_buffer[byte_idx] = self.bit_buffer[byte_idx][::-1]

    def increment_bit_pointer(self):
        self.bit_pointer += 1
        if self.bit_pointer > 7:
            self.increment_byte_pointer()
            self.bit_pointer = 0

    def increment_byte_pointer(self):
        self.byte_pointer += 1
        if self.byte_pointer == len(self.bit_buffer):
            self.byte_pointer = 0

    def read_bits(self, bit_amount, return_type=bool):
        if return_type is bool:
            bits = []
        if return_type in (int, bytes):
            bits = b""
        for _ in range(bit_amount):
            if return_type is bool:
                bits.append(self[self.byte_pointer:self.bit_pointer])
            if return_type in (int, bytes):
                bits = bits+self[self.byte_pointer][self.bit_pointer].to_bytes(1, "big")
            self.increment_bit_pointer()
        if return_type in (int, bytes):
            bits = int(bits, 2)
            if return_type is bytes:
                bits = bits.to_bytes(misc.length(bits), "big")
        return bits

    def set_byte_pointer(self, pointer_val):
        """
        set_byte_pointer
            set_byte_pointer sets the buffer's byte pointer to the int() result of pointer_val. Will raise an IndexError
              if the value is not within [0, len(self.bit_buffer)).

        :param pointer_val:
            Sets the current buffer's byte pointer to the int() result of pointer_val. Will raise an IndexError if
                pointer_val is not within [0, len(self.bit_buffer)). bit_buffer is used over byte_buffer because if the
                user is not updating the bytes with the update_bytes flag while calling write_bit, using the length of
                byte_buffer would be inaccurate.

        :return None:
        """
        if 0 <= pointer_val < len(self.bit_buffer):
            self.byte_pointer = int(pointer_val)
        else:
            raise IndexError('cannot point to nonexistent byte {} in this buffer'.format(pointer_val))

    def set_bit_pointer(self, pointer_val):
        """
        set_bit_pointer
            set_bit_pointer sets the buffer's bit pointer to the int() result of pointer_val. Will raise an IndexError
                if the value is not within [0, 8).

        :param pointer_val:
            Sets the current buffer's bit pointer to the int() result of pointer_val. Will raise an IndexError if
                pointer_val is not within [0, 8).

        :return None:
        """
        if 0 <= pointer_val < 8:
            self.bit_pointer = int(pointer_val)
        else:
            raise IndexError('cannot point to nonexistent bit {}. There are only 8 bits in a byte!'.format(pointer_val))

    def update_byte_buffer(self):
        """
        update
            Updates byte_buffer with the binary values in bit_buffer.

        :return None:
        """
        self.byte_buffer = [int(byte, 2) for byte in self.bit_buffer]

    def update_bit_buffer(self):
        self.bit_buffer = [bin(char)[2:] for char in self.byte_buffer]

    def flush(self):
        self.bit_buffer = []

    def get_bytes(self):
        """
        get_bytes
            Updates byte_buffer and returns byte_buffer.
        :return bytes:
        """
        if len(self.bit_buffer) == 0:
            return []
        self.update_byte_buffer()
        if self.return_type is list:
            return self.byte_buffer
        elif self.return_type is int:
            return int(b''.join(self.bit_buffer), 2)
        elif self.return_type is bytes:
            if not self.return_separate_bytes:
                if self.auto_pad:
                    self.byte_buffer = b''.join(byte.rjust(8, self.padding) for byte in self.bit_buffer)
                else:
                    self.byte_buffer = misc.chunk_string(b''.join(byte for byte in self.bit_buffer), 8)
                    self.byte_buffer = b''.join(int(byte, 2).to_bytes(1, 'big') for byte in self.byte_buffer)
        return self.byte_buffer