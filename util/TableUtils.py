import ItemTable
import RarityBracket
from util import BufferUtils
from util import misc


def pack_to_bits(table, *args, **kwargs):

    data_buffer = BufferUtils.BitBuffer()
    data_buffer.write_bit(len(table.table_name), 7)

    for char in table.table_name[:128]:
        char_table_idx, char_idx = misc.get_encode_char_idx(char)
        data_buffer.write_bit(char_table_idx, 2)
        data_buffer.write_bit(char_idx, 5)
    data_buffer.write_bit(table.allocated_rarity_slots, 30)
    data_buffer.write_bit(len(table.brackets), 8)

    if len(data_buffer[len(data_buffer)-1]) < 8:

        while len(data_buffer[len(data_buffer)-1]) < 8:
            data_buffer.write_bit(True)

    else:
        data_buffer.write_bit(255, 8)
    header_data = bytes(data_buffer)
    header_len = int(len(header_data)).to_bytes(1, "big")
    bracket_data = b""

    for bracket in table.brackets:
        bracket_bytes = bytes(table.brackets[bracket])
        bracket_data += int(len(bracket_bytes)).to_bytes(2, "big")
        bracket_data += bracket_bytes
    return header_len+header_data+bracket_data


def unpack_table_from_bits(bytestring: bytes):

    data_buffer = BufferUtils.BitBuffer(byte_array=bytestring, auto_pad=True)
    data_buffer.set_byte_pointer(0)
    header_len = data_buffer.read_bits(8, int)
    table_name_len = data_buffer.read_bits(7, int)
    table_name = ""

    for _ in range(table_name_len):
        char_table_idx = data_buffer.read_bits(2, int)
        char_idx = data_buffer.read_bits(5, int)
        table_char = misc.get_char_for_idx(char_table_idx, char_idx)
        table_name += table_char
    allocated_table_slots = data_buffer.read_bits(30, int)
    child_bracket_amount = data_buffer.read_bits(8, int)
    table = ItemTable.ItemTable(table_name=table_name, allocated_rarity_slots=allocated_table_slots)
    bracket_data = bytestring[header_len+1:]
    pointer = 0

    try:

        for _ in range(child_bracket_amount):
            itemdef_len = int.from_bytes(bracket_data[pointer:pointer+2], "big")
            pointer += 2
            data_end_pointer = pointer+itemdef_len
            bracket = RarityBracket.RarityBracket.unpack(bracket_data[pointer:data_end_pointer])
            table.add_bracket(bracket)
            pointer = data_end_pointer
    except IndexError:
         pass

    table.init()

    if ItemTable.validate_table(table):
        return table
    total_bracket_slots = 0

    for bracket_id in table.brackets:
        total_bracket_slots += table.brackets[bracket_id].allocated_slots
    raise IOError("Invalid table slot allocation ({}/{}) for table {}".format(total_bracket_slots, table.allocated_rarity_slots, table.table_name))


def to_java(table):

    retstr = "new ItemTable("
    retstr += '"{}", '.format(table.table_name)
    retstr += "{}, ".format(table.allocated_rarity_slots)
    retstr += "\n\t\t\tnew RarityBracket[] {"

    for bracket_id in table.brackets:
        retstr += "\n\t\t\t\t\t{},".format(table.brackets[bracket_id].to_java())
    retstr += "\n\t\t\t\t}"
    retstr += "\n\t\t\t);"

    return retstr


def from_java(string: str):

    string = string.replace("\n", "").replace("\t", "").replace("  ", "").replace("});", "")
    table_name, table_slots = string.split("new RarityBracket")[0].split("(")[1].split(',')[:2]
    table_name = table_name.strip().replace('"', "").replace("'", "")
    table_slots = int(table_slots.strip())
    table = ItemTable.ItemTable(table_name, table_slots)
    bracket_stuff = string.split("new RarityBracket")[2:]
    
    for bracket_string in bracket_stuff:
        bracket_obj = RarityBracket.RarityBracket.from_java(bracket_string)
        table.add_bracket(bracket_obj)
    ItemTable.validate_table(table)
    table.init()
    return table
