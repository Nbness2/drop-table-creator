import RarityBracket
from util import BufferUtils
from util import misc


def pack_to_bits(bracket, *args, **kwargs):

    data_buffer = BufferUtils.BitBuffer()
    data_buffer.write_bit(len(bracket.bracket_name), 6)

    for char in bracket.bracket_name[:64]:
        char_table_idx, char_idx = misc.get_encode_char_idx(char)
        data_buffer.write_bit(char_table_idx, 2)
        data_buffer.write_bit(char_idx, 5)
    data_buffer.write_bit(bracket.allocated_slots, 24)
    data_buffer.write_bit(len(bracket.item_ids)-1, 10)

    for item_id in bracket.item_ids:
        data_buffer.write_bit(item_id, 15)

    if len(data_buffer[len(data_buffer)-1]) < 8:

        while len(data_buffer[len(data_buffer)-1]) < 8:
            data_buffer.write_bit(True)

    else:
        data_buffer.write_bit(255, 8)
    data_buffer.flip_byte(len(data_buffer)-1)
    data = bytes(data_buffer)
    return data


def unpack_from_bits(bytestring: bytes):

    data_buffer = BufferUtils.BitBuffer()
    data_buffer.flush()
    data_buffer.set_buffer(bytestring, True)
    data_buffer.set_byte_pointer(0)
    data_buffer.flip_byte(len(data_buffer)-1)
    bracket_name_length = data_buffer.read_bits(6, int)
    bracket_name = ""

    for _ in range(bracket_name_length):
        bracket_char_table_idx = data_buffer.read_bits(2, int)
        bracket_char_idx = data_buffer.read_bits(5, int)
        bracket_name += misc.get_char_for_idx(bracket_char_table_idx, bracket_char_idx)
    allocated_bracket_slots = data_buffer.read_bits(24, int)
    bracket_item_ids = []
    bracket_ids_length = data_buffer.read_bits(10, int)+1

    for _ in range(bracket_ids_length):
        bracket_item_id = data_buffer.read_bits(15, int)
        bracket_item_ids.append(bracket_item_id)
    bracket = RarityBracket.RarityBracket(
        bracket_name=bracket_name,
        allocated_slots=allocated_bracket_slots,
        item_ids=tuple(bracket_item_ids)
    )
    return bracket


def to_java(bracket):

    retstr = 'new RarityBracket('
    retstr += '"{}", '.format(bracket.bracket_name)
    retstr += "{}, ".format(bracket.allocated_slots)
    retstr += "new short[] { "

    for idx, item_id in enumerate(bracket.item_ids):
        retstr += "{}{} ".format(item_id, "" if idx+1 == len(bracket.item_ids) else ",")
    retstr += "})"
    return retstr


def from_java(string: str):

    main_args = string.split("(")[1].replace(")", "")
    _, bracket_name, main_args = main_args.split('"')
    bracket_slots, bracket_items = main_args.split("new int[]")
    bracket_slots = int(bracket_slots.strip().replace(",", ""))
    bracket_items = bracket_items.replace("{", "").replace("}", "").replace(" ", "").split(",")
    bracket_items.remove("")
    bracket_items = tuple([int(item_id) for item_id in bracket_items])
    return RarityBracket.RarityBracket(bracket_name, bracket_slots, bracket_items)