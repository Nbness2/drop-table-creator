import re


class NotEqualsError(BaseException):
    pass


class NotContainedInError(BaseException):
    pass


class NotLessThanError(BaseException):
    pass


class NotLessThanEqualError(BaseException):
    pass


class NotGreaterThanError(BaseException):
    pass


class NotGreaterThanEqualError(BaseException):
    pass


class MaxLenBrokenError(BaseException):
    pass


table_characters = " s9hP,=\"f8ap;T-b'NSey|ODA%#U[@KtXu$B+IigCo?jQ!r6n_><Wv*4/VE2{15c\\GxM}HlYZdR3]J0FL.:qmz7&kw^"


test_java_array = """new ItemTable("BASE MBOX TABLE", 78,
                new RarityBracket[] {
                        new RarityBracket("SUPER COMMON", 12, new int[] {15020}),
                        new RarityBracket("VERY COMMON", 11, new int[] {3140, 4708, 4710, 3176, 1187, 19281, 11732, 4585, 4087}),
                        new RarityBracket("COMMON", 10, new int[] {4151, 6735, 6739, 4716, 4722, 4726, 4734, 4755, 12985, 1771, 19284, 13734}),
                        new RarityBracket("KINDA UNCOMMON", 9, new int[] {6731, 6733, 4714, 4718, 4724, 4734, 4753, 12988, 19287, 19339, 11690}),
                        new RarityBracket("UNCOMMON", 8, new int[] {10296, 11335, 4712, 4728, 4736, 4738, 4745, 8842, 4747, 12991, 1765, 15259, 11708, 11718}),
                        new RarityBracket("VERY UNCOMMON", 7, new int[] {6737, 8839, 8840, 14479, 4751, 4757, 12994, 19290, 19299, 17273, 11704, 11722, 13263}),
                        new RarityBracket("SUPER UNCOMMON", 6, new int[] {13864, 13867, 4720, 4730, 4749, 4759, 15018, 15019, 12997, 1767, 1772, 17726, 11663, 11664, 11665, 11706, 11720}),
                        new RarityBracket("KINDA RARE", 5, new int[] {13861, 13873, 13876, 1165, 14484, 13000, 14637, 19302, 19338, 19340, 18346, 11702}),
                        new RarityBracket("RARE", 4, new int[] {11808, 13858, 13870, 13890, 13893, 13896, 13926, 13929, 15017, 15098, 10551, 19336, 19337, 13752, 15825}),
                        new RarityBracket("VERY RARE", 3, new int[] {13884, 13887, 13902, 13923, 13978, 13003, 19669, 19293, 6570, 13746}),
                        new RarityBracket("SUPER RARE", 2, new int[] {13899, 13905, 15497, 14579, 19341, 19342, 19344, 19343, 19345, 13750, 13754}),
                        new RarityBracket("L E G E N D A R Y", 1, new int[] {17241, 13006, 18747, 19305, 13748}),
                    }
                );"""

test_ugly_array = """{13873, 13003, 15825, 11732, 3140, 15020, 4745, 19290, 15020, 4745, 13905,
    11720, 19287, 4724, 11335, 13861, 12985, 18346, 4732, 14637, 6739, 4753, 15259, 4736, 1765, 11664, 12985,
    15018, 15018, 8840, 4753, 13870, 19290, 4732, 4710, 14479, 1187, 4714, 19340, 19281, 19337, 3140, 4585,
    3140, 4714, 11704, 6733, 15017, 8842, 14484, 4712, 3140, 11702, 4087, 13263, 11718, 1187, 8840, 4151, 6739,
    4585, 4714, 19340, 13858, 4730, 11690, 4151, 4736, 19302, 4732, 4749, 4751, 8842, 19284, 15018, 6735, 19287,
    13884, 4712, 11690, 19340, 1772, 4747, 6733, 4732, 1765, 13896, 13884, 4726, 19344, 15017, 13263, 4728,
    4718, 15017, 4718, 6570, 19339, 13000, 4722, 11690, 13893, 6737, 4087, 15259, 15017, 11702, 13876, 13748,
    15019, 4087, 13754, 18747, 13006, 11718, 8840, 1772, 8842, 10296, 6737, 4087, 1767, 17726, 12988, 1771,
    13752, 19299, 8842, 19338, 10296, 1765, 12997, 4712, 19337, 4745, 13890, 4726, 12997, 4730, 19669, 4759,
    13902, 12994, 13734, 13734, 4718, 13929, 4151, 13926, 4747, 6739, 13752, 4708, 19281, 4724, 19287, 8839,
    11732, 11663, 1165, 8842, 11664, 19299, 4753, 6570, 4718, 19336, 4747, 12991, 4726, 12988, 19344, 11720,
    13870, 4708, 6735, 13876, 6733, 13926, 6735, 4738, 4087, 4753, 1765, 12985, 8840, 4755, 6731, 1772, 4585,
    4759, 11722, 19339, 11702, 3176, 12985, 6739, 17273, 15020, 1771, 4722, 11708, 13902, 12988, 19284, 15020,
    1771, 4087, 13873, 11808, 4151, 4712, 17273, 11663, 4714, 15098, 13902, 13905, 11732, 6739, 11704, 15098,
    4718, 13003, 4716, 6731, 4720, 11665, 4087, 12985, 18346, 8839, 4734, 13750, 4745, 4757, 4712, 12991, 1771,
    4728, 12994, 4751, 11690, 19336, 19290, 4714, 4730, 4738, 18346, 10551, 10551, 10551, 10551, 4757, 13734,
    1771, 19281, 4708, 13870, 19340, 4151, 13899, 1187, 19281, 4732, 14479, 4755, 13864, 19338, 4710, 19336,
    14579, 15019, 1772, 1771, 4759, 13876, 1767, 4757, 4730, 11335, 6570, 4753, 4755, 6733, 11663, 14479,
    1765, 4755, 4718, 6733, 12994, 15497, 12988, 1187, 13750, 19339, 1772, 11663, 18346, 19302, 19287, 1771,
    11706, 12994, 1772, 11335, 4718, 4751, 13873, 4726, 19287, 15825, 4151, 4755, 19337, 11732, 6735, 13734,
    19284, 13864, 11732, 13929, 19339, 4732, 11722, 4708, 4726, 19339, 13890, 19339, 1187, 11702, 18346, 17726,
    19281, 13746, 4745, 14637, 8839, 19287, 6735, 4734, 12997, 19336, 1767, 19669, 1765, 13864, 15020, 15259,
    11722, 13876, 4747, 4720, 6739, 19343, 13867, 4755, 3176, 4736, 4734, 4151, 11335, 11732, 4734, 6731, 13873,
    4751, 4745, 4736, 13734, 4087, 13263, 1187, 13754, 15018, 8839, 19290, 4708, 11690, 14484, 4151, 11722,
    13263, 11665, 12988, 11720, 15497, 13896, 19339, 14479, 12985, 4738, 4730, 19281, 4736, 17273, 19338, 4738,
    11690, 13858, 11665, 4749, 4736, 4757, 11690, 4722, 6737, 19345, 4710, 12991, 13896, 13000, 4720, 6737,
    13899, 13864, 4708, 4712, 3176, 11663, 15259, 19281, 4710, 10296, 13746, 4585, 13893, 6731, 11708, 4732,
    13890, 19284, 11704, 4722, 4734, 6737, 13000, 13858, 4716, 1765, 4728, 4755, 19302, 13263, 6735, 11718,
    6733, 4724, 6737, 4716, 4726, 13858, 11690, 12985, 3140, 3140, 13861, 13929, 15020, 8842, 8842, 11665,
    10296, 15259, 12997, 19299, 11708, 15020, 14479, 1187, 4151, 4755, 4724, 4753, 10296, 15098, 19281, 8839,
    1767, 4757, 4716, 6739, 11718, 4759, 1765, 11708, 19287, 8839, 19302, 19299, 14479, 12997, 4724, 4753,
    11732, 15259, 12988, 12985, 6735, 4708, 4736, 4757, 14637, 15259, 4728, 17726, 4749, 11335, 12991, 15019,
    12997, 19338, 11808, 13884, 19290, 4716, 4585, 4708, 11665, 19284, 4712, 13929, 4753, 11706, 13752, 19669,
    13926, 13003, 11708, 11732, 15020, 17273, 11706, 13000, 19293, 19338, 19305, 4738, 12988, 4714, 19284,
    10296, 4730, 11708, 13923, 4734, 4728, 4726, 4751, 11808, 10296, 19290, 4087, 3176, 4718, 13890, 3176, 6735,
    11690, 3176, 13746, 4720, 19299, 19281, 11704, 4716, 12994, 3176, 15020, 13734, 3140, 1187, 13978, 4710,
    11722, 13867, 3176, 4749, 4728, 13867, 15019, 11708, 4738, 19293, 4728, 4722, 4747, 19343, 4726, 12985,
    6739, 13752, 6739, 13734, 12994, 13864, 14637, 13923, 19341, 13978, 11706, 4724, 15098, 11664, 4724, 11732,
    13870, 19284, 11732, 4710, 4745, 15020, 4726, 3140, 1165, 15259, 4087, 17273, 4732, 4714, 13861, 4759, 4747,
    11704, 1767, 12991, 13867, 4710, 13887, 1771, 11664, 4722, 12991, 4747, 13978, 4755, 13000, 4751, 13861,
    4708, 13896, 11722, 13873, 13263, 1771, 4751, 3140, 11704, 4732, 4734, 14637, 12985, 12994, 11664, 4722,
    11718, 15825, 11720, 1187, 4732, 6739, 11706, 4753, 4736, 17726, 19299, 13734, 13263, 11335, 11706, 13887,
    11718, 4757, 4712, 11732, 4759, 13864, 11665, 19340, 11704, 4585, 4710, 15018, 11722, 12988, 10296, 11720,
    13893, 13734, 17726, 4585, 19293, 11335, 4585, 6735, 1187, 8840, 19337, 4714, 19281, 6733, 1165, 1771, 8840,
    4710, 13926, 4585, 6731, 15019, 4720, 15825, 4745, 11702, 3140, 11708, 3140, 8839, 14484, 19342, 4755,
    11718, 15020, 6735, 19284, 4720, 4724, 14579, 6731, 6733, 4728, 6731, 4716, 19284, 19341, 3176, 4585, 4708,
    17273, 1165, 11335, 4734, 6731, 19287, 13923, 4738, 6733, 4716, 13861, 13893, 6737, 3176, 11718, 4722,
    11664, 13887, 17273, 4585, 4734, 4747, 19302, 1187, 4716, 4151, 4087, 4718, 3176, 4749, 4738, 19339, 14484,
    13867, 13734, 4722, 15019, 11808, 4708, 17726, 19287, 11720, 4714, 1165, 19339, 13867, 4716, 11724, 12988,
    8842, 19299, 8840, 14484, 19342, 4710, 19281, 19290, 4722, 4749, 11663, 12991, 4724, 4710, 13876, 19345,
    15020, 4726, 19284, 6731, 14479, 1767, 12991, 15018}"""


def load_itemdb(filepath):

    with open(filepath) as db:
        return {item.split(',')[0].split(' = ')[1].strip(): item.split(',')[1].split(' = ')[1].strip() for item in db.readlines()}


def chunk_string(string, chunksize):

    search = '.{1,' + str(chunksize) + '}'

    if type(string) is bytes:
        search = search.encode()
    return re.findall(search, string)


def length(data):

    if type(data) is int:
        return int(data.bit_length() + 7) // 8
    else:
        return len(data)


def replace_str_char(string, idx, replacement):

    if type(string) is str:
        replaced = '{}{}{}'.format(string[:idx], replacement, string[idx + 1:])
    elif type(string) is bytes:
        replaced = b''.join([string[:idx], replacement, string[idx + 1:]])
    else:
        raise TypeError("Cannot replace a character in type {}".format(type(string)))
    return replaced


def input_loop(
        *, loop_message="Loop message here: ", term_message="", must_confirm_entry=True, return_datatype=str,
        allow_duplicates=True, greaterthan=None, lessthan=None
):

    results = []
    entry_confirmed = True
    can_append = False

    while True:
        result = input(loop_message)

        if result == term_message:
            return tuple(results)

        if must_confirm_entry:
            entry_confirmed_s = input("Confirm entry \"{}\" [y/n]: ".format(result))

            if entry_confirmed_s:
                entry_confirmed = True if entry_confirmed_s[0].lower() == "y" else False

        if entry_confirmed:

            try:
                data = return_datatype(result)
                can_append = True

                if data in results and not allow_duplicates:
                    print("Duplicates not allowed ({})... ".format(result), end="")
                    can_append = False

                if greaterthan is not None:

                    if data > greaterthan:
                        results.append(data)
                    else:
                        print("Entry must be greater than {}... ".format(data), end="")
                        can_append = False

                if lessthan is not None:

                    if data < lessthan:
                        results.append(data)
                    else:
                        print("Entry must be less than {}... ".format(data), end="")
                        can_append = False

                if can_append:
                    results.append(data)
            except ValueError:
                print("Invalid entry {}... ".format(result), end="")


def iterable_input_loop(
        iterable, *, loop_message="Loop message here", must_confirm_entry=True, allow_duplicates=True,
        return_datatype=str
):
    """
    :param iterable:
        The iterable that the loop matches results to in the order a for loop would read it (recommended to use
        an iterable type with a constant order like list, not dict)
    :param loop_message:
        The message that shows to request input. Will automatically add "(for item {}): " if no "{}" is found
    :param must_confirm_entry:
        The boolean that dictates whether you must confirm your entry or not
    :param allow_duplicates:
        The boolean that dictates whether you will allow exact duplicates as entries
    :param return_datatype:
        The forced datatype that goes into the results
    :return:
    """

    if "{}" not in loop_message:
        loop_message += "(for item {}): "
    results = []
    entry_confirmed = True

    for item in iterable:
        item_complete = False

        while not item_complete:
            result = input(loop_message.format(item))

            if must_confirm_entry:
                entry_confirmed_s = input("Confirm entry \"{}\" for \"{}\" [y/n]: ".format(result, item))

                if entry_confirmed_s:
                    entry_confirmed = True if entry_confirmed_s[0].lower() == "y" else False

            if entry_confirmed:

                try:
                    data = return_datatype(result)

                    if data in results and not allow_duplicates:
                        print("Duplicates not allowed ({})... ".format(result), end="")
                    else:
                        results.append(return_datatype(result))
                        item_complete = True
                except ValueError:
                    print("Invalid entry {}... ".format(result), end="")
    return tuple(results)


def advanced_input(*, message, field, expected_datatype=None, mustconfirm=False,
                   mustequal=None, mustbein=None, lessthan=None, greaterthan=None, maxlen=None, lessthanequal=None,
                   greaterthanequal=None):

    if mustconfirm:
        confirm_message = "Confirm value \"{}\" for ({} \"{}\") (y/n): "

    if expected_datatype is not None:
        typename = repr(expected_datatype).split("'")[1]

    while True:

        try:
            data = input(message)

            if mustconfirm:

                if input(confirm_message.format(data, typename, field))[0].lower() == "y":
                    pass
                else:
                    continue

            if expected_datatype is not None:
                data = expected_datatype(data)

            if lessthanequal is not None:

                if data <= lessthanequal:
                    return data
                else:
                    raise NotLessThanEqualError()

            if greaterthanequal is not None:

                if data >= greaterthanequal:
                    return data
                else:
                    raise NotGreaterThanEqualError()

            if mustequal is not None:

                if data == mustequal:
                    return data
                else:
                    raise NotEqualsError()

            if mustbein is not None:

                if data in mustbein:
                    return data
                else:
                    raise NotContainedInError()

            if lessthan is not None:

                if not data < lessthan:
                    raise NotLessThanError()

            if greaterthan is not None:

                if not data > greaterthan:
                    raise NotGreaterThanError()

            if maxlen and maxlen > 0 and maxlen is not None:

                if not len(data) <= maxlen:
                    raise MaxLenBrokenError()

            return data

        except IndexError:
            print("You must put \"y\" or \"n\" in for an answer")
        except (TypeError, ValueError):
            print("You must put in a proper value for the type {}, not \"{}\"".format(typename, data))
        except NotEqualsError:
            print("Your input must equal {}".format(mustequal))
        except NotContainedInError:
            print("Your input must be contained in {}".format(mustbein))
        except NotLessThanError:
            print("Your input must be less than {}".format(lessthan))
        except NotGreaterThanError:
            print("Your input must be greater than {}".format(greaterthan))
        except MaxLenBrokenError:
            print("The length of your input must be less than or equal to {}".format(maxlen))
        except NotLessThanEqualError:
            print("Your input must be less than or equal to {}".format(lessthanequal))
        except NotGreaterThanEqualError:
            print("Your input must be greater than or equal to {}".format(greaterthanequal))


def confirm_data_type(data, expected_type, field_name):

    if type(data) == expected_type:
        return data
    raise TypeError("Invalid type ({}) for field {}, must be {}".format(type(data), field_name, expected_type))


tables = chunk_string(table_characters, 32)


def get_encode_char_idx(char: str):

    try:
        char = char[0]
        for table_idx, table in enumerate(tables):

            if char in table:
                char_idx = table.index(char)
                return table_idx, char_idx

    except IndexError:
        pass
    return 0, 0


def get_char_for_idx(table_idx: int, char_idx: int):

    try:
        return tables[table_idx][char_idx]
    except IndexError:
        print(table_idx, char_idx, tables)


if __name__ == "__main__":
    print(tables)
