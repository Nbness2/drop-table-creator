import random
from util import misc
from util import BracketUtils


class RarityBracket:

    def __init__(self, bracket_name: str, allocated_slots: int, item_ids: tuple):

        self.bracket_name = misc.confirm_data_type(bracket_name, str, "RarityBracket.rarity_name")[:64]
        self.allocated_slots = misc.confirm_data_type(allocated_slots, int, "RarityBracket.rarity_slots")
        misc.confirm_data_type(item_ids, tuple, "RarityBracket.rarity_item_ids")

        for item in item_ids:
            misc.confirm_data_type(item, int, "RarityBracket.rarity_item_ids[item]")
        self.item_ids = item_ids
        self.bracket_id = None

    def __str__(self):

        return "RarityBracket(\"{}\", {})".format(self.bracket_name, self.allocated_slots)

    def __contains__(self, item: int):

        return item in self.item_ids

    def __eq__(self, other):
        """checks for statistical equality between brackets"""

        if not self.allocated_slots == other.allocated_slots:
            return False

        if not self.item_ids == other.item_ids:
            return False
        return True

    def __bytes__(self):
        """packs the bracket into the latest version bytestring"""
        bytestring = BracketUtils.pack_to_bits(self)
        return bytestring

    def pack(self, version):

        bytestring = BracketUtils.pack_to_bits(self, version)
        return bytestring

    @staticmethod
    def unpack(bytestring: bytes):

        bracket = BracketUtils.unpack_from_bits(bytestring)
        return bracket

    def set_id(self, bracket_id: int):

        self.bracket_id = bracket_id

    def set_bracket_name(self, new_name: str):

        misc.confirm_data_type(new_name, str, "RarityBracket.BracketName")
        self.bracket_name = new_name[:64]

    def pick_item(self, amount=1):

        if amount < 2:
            return random.SystemRandom().choice(self.item_ids)

        picked = []

        for _ in range(amount):
            picked.append(random.SystemRandom().choice(self.item_ids))

    def to_java(self):

        return BracketUtils.to_java(self)

    @staticmethod
    def from_java(string: str):

        return BracketUtils.from_java(string)

    @staticmethod
    def create(allocated_slots: int, ):

        rarity_name = misc.advanced_input(
            message="Input a rarity name: ",
            field="rarity name",
            mustconfirm=False,
            expected_datatype=str
        )
        rarity_slots = misc.advanced_input(
            message="Input the slots for rarity \"{}\" ({} remaining): ".format(rarity_name, allocated_slots),
            field="rarity slots",
            mustconfirm=False,
            expected_datatype=int,
            lessthanequal=allocated_slots,
        )
        rarity_item_ids = misc.input_loop(
            loop_message="Input an item id to add to rarity {} ({} slots): ".format(rarity_name, rarity_slots),
            term_message="",
            must_confirm_entry=False,
            allow_duplicates=False,
            return_datatype=int,
            greaterthan=-1,
            lessthan=19711
        )

        new_bracket = RarityBracket(
            bracket_name=rarity_name,
            allocated_slots=rarity_slots,
            item_ids=rarity_item_ids
        )
        return new_bracket


if __name__ == "__main__":

    tbracket = RarityBracket(
        bracket_name="test",
        allocated_slots=420,
        item_ids=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)
    )
    print(tbracket)
    packed = bytes(tbracket)
    print(tbracket.to_java())
    print("packed", packed)
    unpacked = RarityBracket.unpack(packed)
    print("unpacked", unpacked, unpacked.item_ids)